﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.

function plus()      //function to increase size the iframe
{
    var x = document.getElementById("IFrame").width;
    document.getElementById("IFrame").width =1.2*x;

    var y = document.getElementById("IFrame").height;
    document.getElementById("IFrame").height=1.2*y;
}

function minus() //function to reduce size of the iframe
{
    var x = document.getElementById("IFrame").width;
    document.getElementById("IFrame").width =x/1.2;

    var y = document.getElementById("IFrame").height;
    document.getElementById("IFrame").height=y/1.2;
}

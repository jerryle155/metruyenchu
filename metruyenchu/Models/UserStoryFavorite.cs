﻿using System;
namespace metruyenchu.Models
{
    public class UserStoryFavorite
    {
        public DateTime CreatedAt { get; set; }
        public string UserId { get; set; }
        public virtual ApplicationUser User { get; set; }
        public int StoryId { get; set; }
        public virtual Story Story { get; set; }
    }
}

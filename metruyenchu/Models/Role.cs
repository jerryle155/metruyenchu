﻿using System;
namespace metruyenchu.Models
{
    public class Role
    {
        public const string Member = "Member";
        public const string Moderator = "Moderator";
        public const string Administrator = "Administrator";
    }
}

﻿using System;
using System.Collections.Generic;

namespace metruyenchu.Models
{
    public class Genre
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        //relationships
        public virtual List<GenreStory> GenreStories { get; set; }
    }
}

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace metruyenchu.Models
{
    public class StoryImage
    {
        public int Id { get; set; }

        public byte[] Image { get; set; }

        [Display(Name = "File Name")]
        public string UntrustedName { get; set; }

        [Display(Name = "Note")]
        public string Note { get; set; }

        [Display(Name = "Size (bytes)")]
        [DisplayFormat(DataFormatString = "{0:N0}")]
        public long Size { get; set; }

        [Display(Name = "Uploaded (UTC)")]
        [DisplayFormat(DataFormatString = "{0:G}")]
        public DateTime UploadDT { get; set; }

        // relationships
        public int StoryId {get;set;}
        public virtual Story Story {get;set;}
    }
}

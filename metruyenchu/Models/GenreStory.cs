﻿using System;
namespace metruyenchu.Models
{
    public class GenreStory
    {
        public DateTime CreatedAt { get; set; }
        public int GenreId { get; set; }
        public virtual Genre Genre { get; set; }
        public int StoryId { get; set; }
        public virtual Story Story { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace metruyenchu.Models
{
    public class View
    {
        public int? StoryId { get; set; }
        public int? ChapterId { get; set; }
        public ViewType Type { get; set; }
        public string IpAddress { get; set; }
        public DateTime CreatedAt { get; set; }

    }
}

public class ViewType
{
    public const string Story = "Story";
    public const string Chapter = "Chapter";
}
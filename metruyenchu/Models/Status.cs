﻿using System;
namespace metruyenchu.Models
{
    public class Status
    {
        public const string Verified = "Verified";
        public const string Deactived = "Deactived";
        public const string Banned = "Banned";
        public const string Publish = "Publish";
        public const string Unpublish = "Unpublish";
        public const string Done = "Done";
        public const string Hidden = "Hidden";
        public const string Rejected = "Rejected";
        public const string Approved = "Approved";
    }
}

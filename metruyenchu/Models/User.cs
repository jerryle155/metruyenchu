﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Identity;

namespace metruyenchu.Models
{
    public class ApplicationUser : IdentityUser
    {

        //relationships
        public virtual ICollection<IdentityUserClaim<string>> Claims { get; set; }
        public virtual ICollection<IdentityUserLogin<string>> Logins { get; set; }
        public virtual ICollection<IdentityUserToken<string>> Tokens { get; set; }
        public virtual ICollection<IdentityUserRole<string>> UserRoles { get; set; }

        public virtual ICollection<Story> PublishedStories { get; set; }
        public virtual ICollection<Story> Favorites { get; set; }
        public virtual List<UserStoryFavorite> UserStoryFavorites { get; set; }
    }
}

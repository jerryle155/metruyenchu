﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace metruyenchu.Models
{
    public class Story
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Details { get; set; }
        public string Image { get; set; }
        public string Author { get; set; }
        [EnumDataType(typeof(StoryStatus))]
        public StoryStatus Status { get; set; }
        public int Views { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }

        [Required]
        public string BookName{get;set;}

        public string Slug {get;set;}

        // relationships
        public virtual StoryImage StoryImage { get; set; }
        public string PublisherId { get; set; }
        public virtual ApplicationUser Publisher { get; set; }
        public virtual List<UserStoryFavorite> UserStoryFavorites { get; set; }
        public virtual List<GenreStory> GenreStories { get; set; }

    }

    public enum StoryStatus
    {
        Reviewing = 0,
        Approved = 1,
        Rejected = 2,
        Publish = 3,
        Unpublish = 4,
        Done = 5
    }
}

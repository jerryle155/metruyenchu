﻿using System;
namespace metruyenchu.Models
{
    public class Chapter
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Number { get; set; }
        public string Content { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public string Status { get; set; }

        // relationships
        public string PublisherId { get; set; }
        public virtual ApplicationUser Publisher { get; set; }
        public int StoryId { get; set; }
        public virtual Story Story { get; set; }
    }
}


using metruyenchu.Authorization;
using metruyenchu.Models;
using Microsoft.AspNetCore.Identity;

namespace metruyenchu.Data
{
    public static class UserAndRoleDataInitializer
    {
        public static void SeedData(
            UserManager<ApplicationUser> userManager,
            RoleManager<IdentityRole> roleManager)
        {
            SeedRoles(roleManager);
            SeedUsers(userManager);
        }

        private static void SeedUsers(UserManager<ApplicationUser> userManager)
        {
            if(userManager.FindByEmailAsync("admin@mtcdev.app").Result == null)
            {
                ApplicationUser user = new ApplicationUser();
                user.UserName = "admin@mtcdev.app";
                user.Email = "admin@mtcdev.app";
                user.NormalizedUserName = "admin@mtcdev.app";

                IdentityResult result = userManager.CreateAsync(user, "123456Jerry").Result;
                if(result.Succeeded)
                {
                    userManager.AddToRoleAsync(user,
                                               Constants.AdministratorRole).Wait();
                }
            }

            if(userManager.FindByEmailAsync("manager@mtcdev.app").Result == null)
            {
                ApplicationUser user = new ApplicationUser();
                user.UserName = "manager@mtcdev.app";
                user.Email = "manager@mtcdev.app";
                user.NormalizedUserName = "manager@mtcdev.app";

                IdentityResult result = userManager.CreateAsync(user, "123456Jerry").Result;
                if(result.Succeeded)
                {
                    userManager.AddToRoleAsync(user,
                                               Constants.ManagerRole).Wait();
                }
            }

            if(userManager.FindByEmailAsync("member@mtcdev.app").Result == null)
            {
                ApplicationUser user = new ApplicationUser();
                user.UserName = "member@mtcdev.app";
                user.Email = "member@mtcdev.app";
                user.NormalizedUserName = "member@mtcdev.app";

                IdentityResult result = userManager.CreateAsync(user, "123456Jerry").Result;
                if(result.Succeeded)
                {
                    userManager.AddToRoleAsync(user,
                                               Constants.MemberRole).Wait();
                }
            }
        }

        private static void SeedRoles(RoleManager<IdentityRole> roleManager)
        {
            if(!roleManager.RoleExistsAsync(Constants.AdministratorRole).Result)
            {
                IdentityRole role = new IdentityRole();
                role.Name = Constants.AdministratorRole;
                IdentityResult result = roleManager.CreateAsync(role).Result;
            }
            if(!roleManager.RoleExistsAsync(Constants.ManagerRole).Result)
            {
                IdentityRole role = new IdentityRole();
                role.Name = Constants.ManagerRole;
                IdentityResult result = roleManager.CreateAsync(role).Result;
            }
            if(!roleManager.RoleExistsAsync(Constants.MemberRole).Result)
            {
                IdentityRole role = new IdentityRole();
                role.Name = Constants.MemberRole;
                IdentityResult result = roleManager.CreateAsync(role).Result;
            }
        }

    }
}
﻿using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using BC = BCrypt.Net.BCrypt;
using metruyenchu.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System.Collections.Generic;

namespace metruyenchu.Data
{
    public class MTCDbContext : IdentityDbContext<ApplicationUser>
    {

        public DbSet<Genre> Genres { get; set; }
        public DbSet<Story> Stories { get; set; }
        public DbSet<Chapter> Chapters { get; set; }
        public DbSet<GenreStory> GenreStories { get; set; }
        public DbSet<StoryImage> StoryImages {get;set;}

        public MTCDbContext(DbContextOptions<MTCDbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<ApplicationUser>(b =>
            {
                // Each User can have many UserClaims
                b.HasMany(e => e.Claims)
                    .WithOne()
                    .HasForeignKey(uc => uc.UserId)
                    .IsRequired();

                // Each User can have many UserLogins
                b.HasMany(e => e.Logins)
                    .WithOne()
                    .HasForeignKey(ul => ul.UserId)
                    .IsRequired();

                // Each User can have many UserTokens
                b.HasMany(e => e.Tokens)
                    .WithOne()
                    .HasForeignKey(ut => ut.UserId)
                    .IsRequired();

                // Each User can have many entries in the UserRole join table
                b.HasMany(e => e.UserRoles)
                    .WithOne()
                    .HasForeignKey(ur => ur.UserId)
                    .IsRequired();
            });

            modelBuilder.Entity<Story>()
                .Property(s => s.CreatedAt)
                .HasDefaultValueSql("getdate()");

            modelBuilder.Entity<Story>()
                .Property(s => s.UpdatedAt)
                .HasDefaultValueSql("getdate()");


            modelBuilder.Entity<GenreStory>()
                .HasKey(gs => new { gs.GenreId, gs.StoryId });

            modelBuilder.Entity<UserStoryFavorite>()
                .HasKey(usf => new { usf.UserId, usf.StoryId });

            modelBuilder.Entity<Story>()
                .HasOne(p => p.Publisher)
                .WithMany(b => b.PublishedStories)
                .HasForeignKey(p => p.PublisherId);

            modelBuilder.Entity<Genre>().HasData(
                new Genre { Id = 1, Name = "Comic", Description = "" },
                new Genre { Id = 2, Name = "Education", Description = "" }
                );

            // modelBuilder.Entity<Story>().HasData(
            //     new Story
            //     {
            //         Id = 1,
            //         Title = "Trùng Sinh Chi Ma Giáo Giáo Chủ",
            //         Author = "Phong Thất Nguyệt",
            //         Image = "https://www.nae.vn/ttv/ttv/public/images/story/9b046fce4bdfb219b877ab6dcec0b78e6994b179e411188edbc048155d2a054f.jpg",
            //         CreatedAt = DateTime.Now,
            //         UpdatedAt = DateTime.Now,
            //         Status = StoryStatus.Publish,
            //         PublisherId = "9363c342-f57c-4bbb-993c-1cf80cc40cb7",
            //     },
            //     new Story
            //     {
            //         Id = 2,
            //         Title = "Thâu Hương Cao Thủ",
            //         Author = "Lục Như Hòa Thượng",
            //         Image = "https://www.nae.vn/ttv/ttv/public/images/story/59.jpg",
            //         CreatedAt = DateTime.Now,
            //         UpdatedAt = DateTime.Now,
            //         Status = StoryStatus.Publish,
            //         PublisherId = "9363c342-f57c-4bbb-993c-1cf80cc40cb7"
            //     },
            //     new Story
            //     {
            //         Id = 3,
            //         Title = "Xuyên Việt Tầm Tiên Đạo Nhân",
            //         Author = "Ly Trần Đạo Nhân",
            //         Image = "https://www.nae.vn/ttv/ttv/public/images/story/eafbd49fee9d89c5d5674f5bc11fcb3088542e22ddfe9ed7897ddfc3e6d8237f.jpg",
            //         CreatedAt = DateTime.Now,
            //         UpdatedAt = DateTime.Now,
            //         Status = StoryStatus.Publish,
            //         PublisherId = "9363c342-f57c-4bbb-993c-1cf80cc40cb7"
            //     },
            //     new Story
            //     {
            //         Id = 4,
            //         Title = "Thánh Vũ Tinh Thần",
            //         Author = "Loạn Thế Cuồng Đao",
            //         Image = "https://www.nae.vn/ttv/ttv/public/images/story/434ce0b0be87a92900b8ac55287e2bbf883cdc1a021079126db710a3465aecae.jpg",
            //         CreatedAt = DateTime.Now,
            //         UpdatedAt = DateTime.Now,
            //         Status = StoryStatus.Publish,
            //         PublisherId = "9363c342-f57c-4bbb-993c-1cf80cc40cb7"
            //     },
            //     new Story
            //     {
            //         Id = 5,
            //         Title = "Chí Tôn Trùng Sinh",
            //         Author = "Thảo Căn",
            //         Image = "https://www.nae.vn/ttv/ttv/public/images/story/2b79af97e65c40cee08ee569741a6a80aca5ccd9cafb7a763b2fe11087ccfe0e.jpg",
            //         CreatedAt = DateTime.Now,
            //         UpdatedAt = DateTime.Now,
            //         Status = StoryStatus.Publish,
            //         PublisherId = "9363c342-f57c-4bbb-993c-1cf80cc40cb7"
            //     },
            //     new Story
            //     {
            //         Id = 6,
            //         Title = "Nhất Kiếm Phá Đạo",
            //         Author = "Liền Thiên Hồng",
            //         Image = "https://www.nae.vn/ttv/ttv/public/images/story/7536fa9e1a1321ff8433d6dc8a6bf38bfce5706aca9033611189bde97173aa6c.jpg",
            //         CreatedAt = DateTime.Now,
            //         UpdatedAt = DateTime.Now,
            //         Status = StoryStatus.Publish,
            //         PublisherId = "9363c342-f57c-4bbb-993c-1cf80cc40cb7"
            //     },
            //     new Story
            //     {
            //         Id = 7,
            //         Title = "Bạch Ngân Bá Chủ",
            //         Author = "Túy Hổ",
            //         Image = "https://www.nae.vn/ttv/ttv/public/images/story/31ac1ead43d823dd3a37f18c2e27f82ed35c225114771a0412cfb7057d241b87.jpg",
            //         CreatedAt = DateTime.Now,
            //         UpdatedAt = DateTime.Now,
            //         Status = StoryStatus.Publish,
            //         PublisherId = "9363c342-f57c-4bbb-993c-1cf80cc40cb7"
            //     },
            //     new Story
            //     {
            //         Id = 8,
            //         Title = "Đao Trấn Tinh Hà",
            //         Author = "Khai Hoang",
            //         Image = "https://www.nae.vn/ttv/ttv/public/images/story/4515452a6182fbd17e54822ee2e43a1ab5d9c13fa6e882fd449e1dd2062be07c.jpg",
            //         CreatedAt = DateTime.Now,
            //         UpdatedAt = DateTime.Now,
            //         Status = StoryStatus.Publish,
            //         PublisherId = "9363c342-f57c-4bbb-993c-1cf80cc40cb7"
            //     },
            //     new Story
            //     {
            //         Id = 9,
            //         Title = "Siêu Cấp Thần Lược Đoạt",
            //         Author = "Kỳ Nhiên",
            //         Image = "https://www.nae.vn/ttv/ttv/public/images/story/d46823ffd8eba0cac20f6b7f7421160296b2f6d17d81d7986cc39e5a3c60e76f.jpg",
            //         CreatedAt = DateTime.Now,
            //         UpdatedAt = DateTime.Now,
            //         Status = StoryStatus.Publish,
            //         PublisherId = "9363c342-f57c-4bbb-993c-1cf80cc40cb7"
            //     },
            //     new Story
            //     {
            //         Id = 10,
            //         Title = "Thôn Thiên Ký",
            //         Author = "Phong Thanh Dương",
            //         Image = "https://www.nae.vn/ttv/ttv/public/images/story/163.jpg",
            //         CreatedAt = DateTime.Now,
            //         UpdatedAt = DateTime.Now,
            //         Status = StoryStatus.Publish,
            //         PublisherId = "9363c342-f57c-4bbb-993c-1cf80cc40cb7"
            //     },
            //     new Story
            //     {
            //         Id = 11,
            //         Title = "Thần Cấp Đại Ma Đầu",
            //         Author = "Lạp Mỗ",
            //         Image = "https://www.nae.vn/ttv/ttv/public/images/story/16475.jpg",
            //         CreatedAt = DateTime.Now,
            //         UpdatedAt = DateTime.Now,
            //         Status = StoryStatus.Publish,
            //         PublisherId = "9363c342-f57c-4bbb-993c-1cf80cc40cb7"
            //     },
            //     new Story
            //     {
            //         Id = 12,
            //         Title = "Vĩnh Hằng Chí Tôn",
            //         Author = "Kiếm Du Thái Hư",
            //         Image = "https://www.nae.vn/ttv/ttv/public/images/story/6567f152229a03110d0dea4b82ed0d22147427f89f0397343efcd7169d43b6b4.jpg",
            //         CreatedAt = DateTime.Now,
            //         UpdatedAt = DateTime.Now,
            //         Status = StoryStatus.Publish,
            //         PublisherId = "9363c342-f57c-4bbb-993c-1cf80cc40cb7"
            //     },
            //     new Story
            //     {
            //         Id = 13,
            //         Title = "Đại Hạ Kỷ",
            //         Author = "Bác Diệu",
            //         Image = "https://www.nae.vn/ttv/ttv/public/images/story/ebb7c0a7a5b067adfa25e5fdaddd48ecda8c369b243a11a061062912e1455aba.jpg",
            //         CreatedAt = DateTime.Now,
            //         UpdatedAt = DateTime.Now,
            //         Status = StoryStatus.Publish,
            //         PublisherId = "9363c342-f57c-4bbb-993c-1cf80cc40cb7"
            //     },
            //     new Story
            //     {
            //         Id = 14,
            //         Title = "Ngũ Hành Ngự Thiên",
            //         Author = "Binh Sĩ Ất",
            //         Image = "https://www.nae.vn/ttv/ttv/public/images/story/3531ebe452c957062c3d1a409c5a16a3e18ca57ecd572d36db2823ecbd6e183e.jpg",
            //         CreatedAt = DateTime.Now,
            //         UpdatedAt = DateTime.Now,
            //         Status = StoryStatus.Publish,
            //         PublisherId = "9363c342-f57c-4bbb-993c-1cf80cc40cb7"
            //     },
            //     new Story
            //     {
            //         Id = 15,
            //         Title = "Ngã Tu Đích Khả Năng Thị Giả Tiên",
            //         Author = "Minh Nguyệt Địa Thượng Sương",
            //         Image = "https://www.nae.vn/ttv/ttv/public/images/story/10ee075f8769c7c0d0eea0d6e35398b2928a8080419b448b276bb0a3d56db6a2.jpg",
            //         CreatedAt = DateTime.Now,
            //         UpdatedAt = DateTime.Now,
            //         Status = StoryStatus.Publish,
            //         PublisherId = "9363c342-f57c-4bbb-993c-1cf80cc40cb7"
            //     });

            // modelBuilder.Entity<GenreStory>().HasData(
            //     new GenreStory { CreatedAt = DateTime.Now, StoryId = 1, GenreId = 1 },
            //     new GenreStory { CreatedAt = DateTime.Now, StoryId = 2, GenreId = 1 },
            //     new GenreStory { CreatedAt = DateTime.Now, StoryId = 3, GenreId = 1 },
            //     new GenreStory { CreatedAt = DateTime.Now, StoryId = 4, GenreId = 1 },
            //     new GenreStory { CreatedAt = DateTime.Now, StoryId = 5, GenreId = 1 },
            //     new GenreStory { CreatedAt = DateTime.Now, StoryId = 6, GenreId = 1 },
            //     new GenreStory { CreatedAt = DateTime.Now, StoryId = 7, GenreId = 1 },
            //     new GenreStory { CreatedAt = DateTime.Now, StoryId = 8, GenreId = 1 },
            //     new GenreStory { CreatedAt = DateTime.Now, StoryId = 9, GenreId = 1 },
            //     new GenreStory { CreatedAt = DateTime.Now, StoryId = 10, GenreId = 1 },
            //     new GenreStory { CreatedAt = DateTime.Now, StoryId = 11, GenreId = 1 },
            //     new GenreStory { CreatedAt = DateTime.Now, StoryId = 12, GenreId = 1 },
            //     new GenreStory { CreatedAt = DateTime.Now, StoryId = 13, GenreId = 1 },
            //     new GenreStory { CreatedAt = DateTime.Now, StoryId = 14, GenreId = 1 },
            //     new GenreStory { CreatedAt = DateTime.Now, StoryId = 15, GenreId = 1 },
            //     new GenreStory { CreatedAt = DateTime.Now, StoryId = 10, GenreId = 2 },
            //     new GenreStory { CreatedAt = DateTime.Now, StoryId = 11, GenreId = 2 },
            //     new GenreStory { CreatedAt = DateTime.Now, StoryId = 12, GenreId = 2 },
            //     new GenreStory { CreatedAt = DateTime.Now, StoryId = 13, GenreId = 2 },
            //     new GenreStory { CreatedAt = DateTime.Now, StoryId = 14, GenreId = 2 },
            //     new GenreStory { CreatedAt = DateTime.Now, StoryId = 15, GenreId = 2 }
            //     );

        }

    }
}
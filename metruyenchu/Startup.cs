using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.EntityFrameworkCore;
using metruyenchu.Data;
using Microsoft.AspNetCore.Identity;
using metruyenchu.Models;
using Microsoft.AspNetCore.Identity.UI.Services;
using metruyenchu.Services;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Authorization;
using metruyenchu.Authorization;
using Microsoft.AspNetCore.Routing;

namespace metruyenchu
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // services.AddControllersWithViews();

            services.AddDbContext<MTCDbContext>(opts =>
            {
                opts
                    .UseLazyLoadingProxies()
                    .UseSqlServer(Configuration.GetConnectionString("Jerry"));
            });

            services.AddIdentity<ApplicationUser, IdentityRole>()
                    .AddRoles<IdentityRole>()
                    .AddEntityFrameworkStores<MTCDbContext>()
                    .AddDefaultTokenProviders();

            services.Configure<DataProtectionTokenProviderOptions>(o =>
                o.TokenLifespan = TimeSpan.FromHours(3));

            services.AddTransient<IEmailSender, EmailSender>();
            services.Configure<AuthMessageSenderOptions>(Configuration);

            services.Configure<IdentityOptions>(options =>
            {
                options.Password.RequireDigit = false;
                options.Password.RequireLowercase = false;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = false;
                options.Password.RequiredLength = 3;

                options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(5);
                options.Lockout.MaxFailedAccessAttempts = 5;
                options.Lockout.AllowedForNewUsers = true;

                options.User.AllowedUserNameCharacters =
                    "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._@+";
                options.User.RequireUniqueEmail = true;
            });

            services.Configure<RouteOptions> (options => {
                options.LowercaseUrls = true;               // Url viết chữ thường
                options.LowercaseQueryStrings = true;       // Query trong Url viết chữ thường
            });

            services.ConfigureApplicationCookie(options =>
            {
                options.Cookie.HttpOnly = true;
                options.ExpireTimeSpan = TimeSpan.FromDays(5);

                options.LoginPath = "/Login";
                options.AccessDeniedPath = "/Admin";
                options.SlidingExpiration = true;
            });
            // services.AddScoped

            services.AddRazorPages();

            services.AddAuthorization(o =>
            {
                o.FallbackPolicy = new AuthorizationPolicyBuilder()
                                    .RequireAuthenticatedUser()
                                    .Build();
            });

            services.AddScoped<IAuthorizationHandler, StoryIsOwnerAuthorizationHandler>();
            services.AddSingleton<IAuthorizationHandler, StoryManagerAuthorizationHandler>();
            services.AddSingleton<IAuthorizationHandler, StoryAdminAuthorizationHandler>();

            // services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            // services.AddDistributedMemoryCache();
            // services.AddSession(options =>
            // {
            //     options.IdleTimeout = TimeSpan.FromMinutes(60);
            //     options.Cookie.HttpOnly = true;
            //     options.Cookie.IsEssential = true;
            // });


        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();
            // app.UseSession();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapRazorPages();
            });
        }
    }
}

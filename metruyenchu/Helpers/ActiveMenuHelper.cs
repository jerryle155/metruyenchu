using System;
using metruyenchu.Models;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace metruyenchu.Helpers
{
    public static class ActiveMenuHelper
    {
        public static string IsActive(this IHtmlHelper htmlHelper, string controller, string action)
        {
            var routeData = htmlHelper.ViewContext.RouteData;

            var routeAction = routeData.Values["action"].ToString();
            var routeController = routeData.Values["controller"].ToString();

            var returnActive = (controller == routeController && (action == routeAction || routeAction == "Details"));

            return returnActive ? "active" : "";
        }

        public static string GetStoryStatusString(int status)
        {
            var selected = "";
            switch (status)
            {
                case (int)StoryStatus.Publish:
                    {
                        selected = "Đang ra";
                        break;
                    }
                default:
                    {
                        selected = "Đang ra";
                        break;
                    }
            }
            return selected;
        }
    }
}

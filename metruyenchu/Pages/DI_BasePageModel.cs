using metruyenchu.Data;
using metruyenchu.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace metruyenchu.Pages
{
    public class DI_BasePageModel : PageModel
    {
        protected MTCDbContext Context {get;}
        protected IAuthorizationService AuthorizationService {get;}
        protected UserManager<ApplicationUser> UserManager{get;}

        public DI_BasePageModel(
            MTCDbContext context,
            IAuthorizationService authorizationService,
            UserManager<ApplicationUser> userManager) : base()
        {
            Context = context;
            UserManager = userManager;
            AuthorizationService = authorizationService;
        }

    }
}


using System.Threading.Tasks;
using metruyenchu.Data;
using metruyenchu.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using System.Linq;
using metruyenchu.Extensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using metruyenchu.Authorization;

namespace metruyenchu.Pages.Stories
{
    [AllowAnonymous]
    public class IndexModel : DI_BasePageModel
    {
        public IndexModel(
            MTCDbContext context,
            IAuthorizationService authorizationService,
            UserManager<ApplicationUser> userManager)
            : base(context, authorizationService, userManager)
        {

        }

        public PaginatedList<Story> PaginatedStories { get; set; }

        public async Task OnGetAsync(string sortBy, int? genre, string status, int? pageIndex)
        {
            // var allowedStatus = new[] { StoryStatus.Approved, StoryStatus.Done, StoryStatus.Publish };
            var stories = from s in Context.Stories select s;

            var isAuthorized = User.IsInRole(Constants.ManagerRole) ||
                               User.IsInRole(Constants.AdministratorRole);

            var currentUserId = UserManager.GetUserId(User);
            if (!isAuthorized)
            {
                stories = stories.Where(s => (s.Status == StoryStatus.Publish || s.Status == StoryStatus.Done || s.Status == StoryStatus.Approved) || s.PublisherId == currentUserId);
            }


            stories = stories.Include(s => s.GenreStories).Include(s => s.StoryImage);
            if (genre != null)
            {
                // stories = stories.Where(s => s.Genres.);

            }

            int pageSize = 6;
            PaginatedStories = await PaginatedList<Story>.CreateAsync(stories.AsNoTracking(), pageIndex ?? 1, pageSize);
        }
    }
}
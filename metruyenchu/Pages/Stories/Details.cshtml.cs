
using System.Threading.Tasks;
using metruyenchu.Authorization;
using metruyenchu.Data;
using metruyenchu.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace metruyenchu.Pages.Stories
{
    [Authorize]
    public class DetailsModel : DI_BasePageModel
    {
        public DetailsModel(
            MTCDbContext context,
            IAuthorizationService authorizationService,
            UserManager<ApplicationUser> userManager)
            : base(context, authorizationService, userManager)
        {

        }

        [BindProperty]
        public Story Story { get; set; }

        public async Task<IActionResult> OnGetAsync(int id)
        {
            Story = await Context.Stories.FirstOrDefaultAsync(s => s.Id == id);

            if (Story == null)
            {
                return NotFound();
            }

            return Page();
        }


        public async Task<IActionResult> OnPostAsync(int id, StoryStatus status)
        {
            var story = await Context.Stories.FirstOrDefaultAsync(s => s.Id == id);

            if (story == null)
            {
                return NotFound();
            }

            var operation = (status == StoryStatus.Approved) ? MTCOperations.Approve : MTCOperations.Reject;

            var isAuthorized = await AuthorizationService.AuthorizeAsync(User, story, operation);

            if (!isAuthorized.Succeeded)
            {
                return Forbid();
            }

            story.Status = status;
            Context.Stories.Update(story);
            await Context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}
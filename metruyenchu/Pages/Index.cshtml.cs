﻿using System;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using metruyenchu.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using metruyenchu.Models;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using metruyenchu.Extensions;

namespace metruyenchu.Pages
{
    [AllowAnonymous]
    public class IndexModel : DI_BasePageModel
    {
        private readonly ILogger<IndexModel> _logger;

        public IndexModel(
            MTCDbContext context,
            IAuthorizationService authorizationService,
            UserManager<ApplicationUser> userManager,
            ILogger<IndexModel> logger)
            : base(context, authorizationService, userManager)
        {
            _logger = logger;
        }

        public IList<Story> HotStories { get; set; }
        public IList<Story> RecentlyStories { get; set; }
        public PaginatedList<Story> PaginatedStories { get; set; }

        public async Task OnGetAsync(int? pageIndex)
        {
            var allowedStatus = new[] { StoryStatus.Approved, StoryStatus.Done, StoryStatus.Publish };
            var stories = from s in Context.Stories where allowedStatus.Contains(s.Status) select s;
            stories = stories.Include(s => s.Publisher).Include(s => s.StoryImage).Include(s => s.GenreStories).ThenInclude(s => s.Genre);

            HotStories = await stories.OrderByDescending(s => s.Views).Take(8).AsNoTracking().ToListAsync();
            RecentlyStories = await stories.OrderByDescending(s => s.UpdatedAt).Take(10).AsNoTracking().ToListAsync();

            int pageSize = 3;
            PaginatedStories = await PaginatedList<Story>.CreateAsync(stories.AsNoTracking(), pageIndex ?? 1, pageSize);
        }
    }
}

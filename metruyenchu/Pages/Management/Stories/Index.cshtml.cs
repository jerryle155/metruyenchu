using System;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using metruyenchu.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using metruyenchu.Models;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using metruyenchu.Extensions;
using Microsoft.AspNetCore.Mvc;
using metruyenchu.Authorization;

namespace metruyenchu.Pages.Management.Stories
{
    [Authorize(Roles = "Administrator, Manager")]
    public class IndexModel : DI_BasePageModel
    {
        private readonly ILogger<IndexModel> _logger;

        public IndexModel(
            MTCDbContext context,
            IAuthorizationService authorizationService,
            UserManager<ApplicationUser> userManager,
            ILogger<IndexModel> logger)
            : base(context, authorizationService, userManager)
        {
            _logger = logger;
        }
        public PaginatedList<Story> PaginatedStories { get; set; }

        public async Task OnGetAsync(int? pageIndex)
        {
            var allowedStatus = new[] { StoryStatus.Reviewing, StoryStatus.Rejected };
            var stories = from s in Context.Stories where allowedStatus.Contains(s.Status) select s;
            stories = stories.Include(s => s.Publisher).Include(s => s.StoryImage).Include(s => s.GenreStories).ThenInclude(s => s.Genre);

            int pageSize = 10;
            PaginatedStories = await PaginatedList<Story>.CreateAsync(stories.AsNoTracking(), pageIndex ?? 1, pageSize);
        }

        public async Task<IActionResult> OnPostAsync(int id, StoryStatus status)
        {
            var story = await Context.Stories.FirstOrDefaultAsync(s => s.Id == id);

            if (story == null)
            {
                return NotFound();
            }

            var operation = (status == StoryStatus.Approved) ? MTCOperations.Approve : MTCOperations.Reject;

            var isAuthorized = await AuthorizationService.AuthorizeAsync(User, story, operation);

            if (!isAuthorized.Succeeded)
            {
                return Forbid();
            }

            story.Status = status;
            Context.Stories.Update(story);
            await Context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}

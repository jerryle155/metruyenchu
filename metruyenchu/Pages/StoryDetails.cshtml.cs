
using System.Threading.Tasks;
using metruyenchu.Authorization;
using metruyenchu.Data;
using metruyenchu.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace metruyenchu.Pages
{
    [AllowAnonymous]
    public class StoryDetailsModel : DI_BasePageModel
    {
        public StoryDetailsModel(
            MTCDbContext context,
            IAuthorizationService authorizationService,
            UserManager<ApplicationUser> userManager)
            : base(context, authorizationService, userManager)
        {

        }

        [BindProperty]
        public Story Story { get; set; }

        public async Task<IActionResult> OnGetAsync(string slug)
        {
            Story = await Context.Stories.FirstOrDefaultAsync(s => s.Slug == slug);

            if (Story == null)
            {
                return NotFound();
            }

            return Page();
        }

    }
}
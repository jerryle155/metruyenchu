
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Threading.Tasks;
using metruyenchu.Data;
using metruyenchu.Extensions;
using metruyenchu.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Hosting;

namespace metruyenchu.Pages.Manage.Stories
{
    [Authorize]
    public class CreateModel : DI_BasePageModel
    {
        private readonly IWebHostEnvironment _webHostEnvironment;

        public CreateModel(
            MTCDbContext context,
            IAuthorizationService authorizationService,
            UserManager<ApplicationUser> userManager, IWebHostEnvironment webHostEnvironment)
            : base(context, authorizationService, userManager)
        {
            _webHostEnvironment = webHostEnvironment;
        }

        // private readonly long _fileSizeLimit = 2097152;
        private readonly string[] _permittedExtensions = { ".jpg", ".png", ".jpeg" };
        private readonly string[] _permittedBookExtensions = { ".pdf" };

        [BindProperty]
        public Story Story { get; set; }

        [BindProperty]
        public BufferedSingleFileUploadDb FileImage { get; set; }

        [BindProperty]
        [Required]
        [Display(Name = "Book PDF")]
        public IFormFile FilePDF {get;set;}

        public IList<Genre> Genres { get; set; }

        [BindProperty]
        public int[] genreSelected { get; set; }
        public SelectList genreOptions { get; set; }

        public void OnGet()
        {
            genreOptions = new SelectList(Context.Genres, nameof(Genre.Id), nameof(Genre.Name));
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            var formFileContent =
                await FileHelpers.ProcessFormFile<BufferedSingleFileUploadDb>(
                    FileImage.FormFile, ModelState, _permittedExtensions,
                    2097152);

            var formPDFContent = await FileHelpers.ProcessFormFile<BufferedSingleFileUploadDb>(
                    FileImage.FormFile, ModelState, _permittedBookExtensions,
                    20097152);

            if (!ModelState.IsValid)
            {

                return Page();
            }

            string uniquePDFFilename = UploadedPDF();

            Story.PublisherId = UserManager.GetUserId(User);
            Story.Status = StoryStatus.Reviewing;
            Story.BookName = uniquePDFFilename;
            Story.Slug = await GetSlug(Story.Title);
            Context.Stories.Add(Story);
            await Context.SaveChangesAsync();
            int id = Story.Id; // Yes it's here

            foreach (var select in genreSelected)
            {
                var gs = new GenreStory
                {
                    StoryId = id,
                    GenreId = select
                };
                Context.GenreStories.Add(gs);
            }
            // var file = new StoryImage()
            // {
            //     Image = memoryStream.ToArray(),
            //     StoryId = id
            // };
            var file = new StoryImage
            {
                Image = formFileContent,
                UntrustedName = FileImage.FormFile.FileName,
                Size = FileImage.FormFile.Length,
                UploadDT = DateTime.UtcNow,
                StoryId = id
            };
            Context.StoryImages.Add(file);
            await Context.SaveChangesAsync();

            return RedirectToPage("../../Index");
        }

        private async Task<string> GetSlug(string title)
        {
            string slug = title.ToSlug();
            Story story;

            story = Context.Stories.Single(p => p.Slug == slug);

            if (story == null)
                return await Task.FromResult(slug);

            for (int i = 2; i < 100; i++)
            {
                story = Context.Stories.Single(p => p.Slug == $"{slug}{i}");

                if (story == null)
                {
                    return await Task.FromResult(slug + i.ToString());
                }
            }

            return await Task.FromResult(slug);
        }

        public async Task<IActionResult> OnPostUploadAsync()
        {
            using (var memoryStream = new MemoryStream())
            {
                await FileImage.FormFile.CopyToAsync(memoryStream);

                // Upload the file if less than 2 MB
                if (memoryStream.Length < 2097152)
                {
                    var file = new StoryImage()
                    {
                        Image = memoryStream.ToArray()
                    };

                    Context.StoryImages.Add(file);

                    await Context.SaveChangesAsync();
                }
                else
                {
                    ModelState.AddModelError("File", "The file is too large.");
                }
            }

            return Page();
        }

        private string UploadedPDF()
        {
            string uniqueFilename = null;
            if(FilePDF != null)
            {
                string uploadFolder = Path.Combine(_webHostEnvironment.WebRootPath, "books");
                uniqueFilename = Guid.NewGuid().ToString() + "_" + FilePDF.FileName;
                string filePath = Path.Combine(uploadFolder, uniqueFilename);
                using(var fileStream = new FileStream(filePath, FileMode.Create))
                {
                    FilePDF.CopyTo(fileStream);
                }
            }

            return uniqueFilename;
        }
    }

    public class BufferedSingleFileUploadDb
    {
        [Required]
        [Display(Name = "Image")]
        public IFormFile FormFile { get; set; }
    }
}
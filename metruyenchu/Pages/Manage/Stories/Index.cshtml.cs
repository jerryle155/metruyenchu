using System;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using metruyenchu.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using metruyenchu.Models;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using metruyenchu.Extensions;

namespace metruyenchu.Pages.Manage.Stories
{
    [Authorize(Roles = "Administrator, Manager")]
    public class IndexModel : DI_BasePageModel
    {
        private readonly ILogger<IndexModel> _logger;

        public IndexModel(
            MTCDbContext context,
            IAuthorizationService authorizationService,
            UserManager<ApplicationUser> userManager,
            ILogger<IndexModel> logger)
            : base(context, authorizationService, userManager)
        {
            _logger = logger;
        }
        public PaginatedList<Story> PaginatedStories { get; set; }

        public async Task OnGetAsync(int? pageIndex)
        {
            var allowedStatus = new[] { StoryStatus.Reviewing, StoryStatus.Rejected };
            var stories = from s in Context.Stories where allowedStatus.Contains(s.Status) select s;
            stories = stories.Include(s => s.Publisher).Include(s => s.StoryImage).Include(s => s.GenreStories).ThenInclude(s => s.Genre);

            int pageSize = 10;
            PaginatedStories = await PaginatedList<Story>.CreateAsync(stories.AsNoTracking(), pageIndex ?? 1, pageSize);
        }
    }
}

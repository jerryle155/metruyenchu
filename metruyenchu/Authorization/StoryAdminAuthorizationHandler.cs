
using System.Threading.Tasks;
using metruyenchu.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authorization.Infrastructure;
using Microsoft.AspNetCore.Identity;

namespace metruyenchu.Authorization
{
    public class StoryAdminAuthorizationHandler : AuthorizationHandler<OperationAuthorizationRequirement, Story>
    {
        protected override Task
            HandleRequirementAsync(AuthorizationHandlerContext context,
                                   OperationAuthorizationRequirement requirement,
                                   Story resource)
        {
            if (context.User == null)
            {
                return Task.CompletedTask;
            }


            // Managers can approve or reject.
            if (context.User.IsInRole(Constants.AdministratorRole))
            {
                context.Succeed(requirement);
            }

            return Task.CompletedTask;
        }
    }
}